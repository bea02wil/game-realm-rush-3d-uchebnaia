﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
[RequireComponent(typeof(Waypoint))]
public class CubeEditor : MonoBehaviour
{   
    TextMesh textMesh;  
    Waypoint waypoint;

    private void Awake()
    {
        waypoint = GetComponent<Waypoint>();
    }

    private void Start()
    {
        textMesh = GetComponentInChildren<TextMesh>();        
    }

    void Update()
    {
        SnapToGrid();
        UpdateLabel();
    }

    private void SnapToGrid()
    {
        int gridSize = waypoint.GridSize;       
        transform.position = new Vector3(
            waypoint.GetGridPos().x * gridSize, 
            0f, waypoint.GetGridPos().y * gridSize); //y - потому что в векторе Vector2Int второе значение Y - а это Z в трансформе
    }

    private void UpdateLabel()
    {       
        string labelText = $"{waypoint.GetGridPos().x},{waypoint.GetGridPos().y}";
        textMesh.text = labelText;
        gameObject.name = labelText;
    }
}
